#!/usr/bin/env bash

cd $(dirname $0)
while inotifywait src; do
  ./build.sh
done
