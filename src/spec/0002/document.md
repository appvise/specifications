# Introduction

This is the internal guide for projects internal to App-Vise. This document
contains both rules and best practices.

{{>keywords-asd-0000.md}}

# Syntax

## Identifiers

Identifiers must use only ASCII letters, digits, underscores (for constants and
structured test method names), and the '\(' sign. Thus each valid identifier
name is matched by the regular expression `[\)\w]+`.

<table>
  <thead>
    <tr>
      <th>Style</th>
      <th>Category</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>PascalCase</td>
      <td>class / interface / type / enum / decorator / type parameters</td>
    </tr>
    <tr>
      <td>camelCase</td>
      <td>variable / parameter / function / method / property / module alias</td>
    </tr>
    <tr>
      <td>snake_case</td>
      <td>node internal modules</td>
    </tr>
    <tr>
      <td>SCREAMING_SNAKE_CASE</td>
      <td>global constants / class constants</td>
    </tr>
    <tr>
      <td>kebab-case</td>
      <td>file and folder names</td>
    </tr>
  </tbody>
</table>

### Abbreveations

Treat abbreviations like acronyms in names as whole words, i.e. use
`loadHttpUrl`, not `loadHTTPURL`, unless required by a platform name (e.g.
`XMLHttpRequest`).

### Dollar sign

Identifiers _should not_ generally use `$`, except when aligning with naming
conventions for third party frameworks. See below for more on using `$` with
`Observable` values.

### Type parameters

Type parameters, like in `Array<T>`, may use a single uppercase character (`T`)
or `PascalCase`

### Test names

Test method names in Closure `testSuite`s and similar xUnit-style test
frameworks may be structured with `_` separators, e.g. `testX_whenY_doesZ()`.

### `_` prefix/suffix

Identifiers _must not_ use `_` as a prefix or suffix.

This also means that `_` _must not_ be used as an identifier by itself (e.g. to
indicate a parameter is unused).

> Tip: If you only need some of the elements from an array (or TypeScript
> tuple), you can insert extra commas in a destructuring statement to ignore
> in-between elements:

```ts
const [a, , b] = [1, 5, 10];  // a <- 1, b <- 10
```

<!-- PAGE BREAK -->

### Imports

Module namespace imports are `lowerCamelCase` while files are `kebab-case`,
which means that imports correctly will not match in casing style, such as:

```ts
import * as fooBar from './foo-bar';
```

Some libraries might commonly use a namespace import prefix that violates this
naming scheme, but overbearingly common open source use makes the violating
style more readable.

### Constants

`SCREAMING_SNAKE_CASE` indicates that a value is _intended_ to not be changed,
and may be used for values that can technically by modified (i.e. values that
are not deeply frozen) to indicate to users that they must not be modified.

```ts
const UNIT_SUFFIXES = [
  'milliseconds': 'ms',
  'seconds'     : 's',
];
// Even though per the rules of JavaScript UNIT_SUFFIXES is mutable, the
// uppercase shows users to not modify it.
```

A constant can also be a `static readonly` property of a class

```ts
class Foo {
  private static readonly MY_SPECIAL_NUMBER = 5;

  bar() {
    return 2 * Foo.MY_SPECIAL_NUMBER;
  }
}
```

If a value can be instantiated more than once over the lifetime of the program,
or if users mutate it in any way, it _should_ use camelCase.

If a value is an arrow function that implements an interface, then it can be
declared camelCase.

## Aliases

When creating a local-scope alias of an existing symbol, use the format of the
existing identifier. The local alias must match the existing naming and format
of the source. For variables use `const` for your local aliases, and for class
fields use the `readonly` attribute.

```ts
const {Foo}    = SomeType;
const CAPACITY = 5;

class Teapot {
  readonly BrewStateEnum = BrewStateEnum;
  readonly CAPACITY      = CAPACITY;
}
```

<!-- PAGE BREAK -->

## Naming style

TypeScript expresses information in types, so names should not be decorated with
information that is included in the type.

Some concrete examples of this rule:

- Do not use trailing or leading underscores for private properties or methods.
- Do not use the `opt_` prefix for optional parameters.
- Do not mark interfaces specially (`IMyInterface` or `MyFooInterface`) unless
    it's idiomatic in its environment. When introducing an interface for a class,
    give it a name that expresses why the interface exists in the first place
    (e.g. `class TodoItem` and `interface TodoItemStorage` if the interface
    expresses the format used for storage/serialization in JSON).
- Suffixing `Observable`s with `$` is a common external convention and can help
    resolve confusion regarding observable values vs concrete values. Judgement
    on whether this is a useful convention is left up to individual teams, but
    _should_ be consistent within projects.

## Descriptive names

Names _must_ be descriptive and clear to a new reader. Do not use abbreviations
that are ambiguous or unfamiliar to readers outside your project, and do not
abbreviate by deleting letters within a word.

- **Exception**: Variables that are in scope for 10 lines or fewer, including
    arguments that are not part of an exported API, may use short (e.g. single
    letter) variable names.

## File encoding: UTF-8

For non-ASCII characters, use the actual Unicode character (e.g. `∞`). For
non-printable characters, the equivalent hex or Unicode escapes (e.g. `\u221e`)
can be used along with an explanatory comment.

```ts
// Perfectly clear, even without a comment.
const units = 'μs';

// Use escapes for non-printable characters.
const output = '\ufeff' + content;  // byte order mark
```

```ts
// Hard to read and prone to mistakes, even with the comment.
const units = '\u03bcs'; // Greek letter mu, 's'

// The reader has no idea what this is.
const output = '\ufeff' + content;
```

<!-- PAGE BREAK -->

## Comments & documentation

### JSDoc vs comments

There are two types of comments, JSDoc (`/** ... */`) and non-JSDoc ordinary
comments (`// ...` or `/* ... */`).

- Use `/** JSDoc */` comments for documentation, i.e. comments a user of the
    code should read.
- Use `// line comments` for implementation comments, i.e. comments that only
    concern the implementation of the code itself.

JSDoc comments are understood by tools (such as editors and documentation
generators), while ordinary comments are only for other humans.

### Document all top-level exports of modules

Use `/** JSDoc */` comments to communicate information to the users of your
code. Avoid merely restating the property or parameter name. You _should also_
document all properties and methods (exported/public or not) whose purpose is
not immediately obvious from their name, as judged by your reviewer.

- **Exception**: Symbols that are only exported to be consumed by tooling, such
    as @NgModule classes, do not require comments.

### Omit comments that are redundant with TypeScript

For example, do not declare types in `@param` or `@return` blocks, do not write
`@implements`, `@enum`, `@private` etc. on code that uses the `implements`,
`enum`, `private` etc. keywords.

### Do not use `@override`

Do not use `@override` in TypeScript source code.

`@override` is not enforced by the compiler, which is surprising and leads to
annotations and implementation going out of sync. Including it purely for
documentation purposes is confusing.

<!-- PAGE BREAK -->

### Make comments that actually add information

For non-exported symbols, sometimes the name and type of the function or
parameter is enough. Code will _usually_ benefit from more documentation than
just variable names though!

- Avoid comments that just restate the parameter name and type, e.g.

    ```ts
    // BAD
    /** @param fooBarService The Bar service for the Foo application. */

    ```

- Because of this rule, `@param` and `@return` lines are only required when they add
    information, and may otherwise be omitted.

    ```ts
    /**
     * POSTs the request to start coffee brewing.
     * @param amountLitres The amount to brew. Must fit the pot size!
     */
    brew(amountLitres: number, logger: Logger) {
      // ...
    }

    ```

### Parameter property comments

A parameter property is when a class declares a field and a constructor
parameter in a single declaration, by marking a parameter in the constructor.
E.g. `constructor(private readonly foo: Foo)`, declares that the class has a foo
field.

To document these fields, use JSDoc's `@param` annotation. Editors display the
description on constructor calls and property accesses.

```ts
/** This class demonstrates how parameter properties are documented. */
class ParamProps {
  /**
   * @param percolator The percolator used for brewing.
   * @param beans The beans to brew.
   */
  constructor(
    private readonly percolator: Percolator,
    private readonly beans: CoffeeBean[]) {}
}
```

```ts
/** This class demonstrates how ordinary fields are documented. */
class OrdinaryClass {
  /** The bean that will be used in the next call to brew(). */
  nextBean: CoffeeBean;

  constructor(initialBean: CoffeeBean) {
    this.nextBean = initialBean;
  }
}
```
<!-- PAGE BREAK -->

### Comments when calling a function

If needed, document parameters at call sites inline using block comments. Also
consider named parameters using object literals and destructuring. The exact
formatting and placement of the comment is not prescribed.

```ts
// Inline block comments for parameters that'd be hard to understand:
new Percolator().brew(/* amountLitres= */ 5);
// Also consider using named arguments and destructuring parameters (in brew's declaration):
new Percolator().brew({amountLitres: 5});
```

```ts
/** An ancient {@link CoffeeBrewer} */
export class Percolator implements CoffeeBrewer {
  /**
   * Brews coffee.
   * @param amountLitres The amount to brew. Must fit the pot size!
   */
  brew(amountLitres: number) {
    // This implementation creates terrible coffee, but whatever.
    // TODO(b/12345): Improve percolator brewing.
  }
}
```

### Place documentation prior to decorators

When a class, method, or property have both decorators like `@Component` and
JsDoc, please make sure to write the JsDoc before the decorator.

- Do not write JsDoc between the Decorator and the decorated statement.

    ```ts
    // BAD
    @Component({
      selector: 'foo',
      template: 'bar',
    })
    /** Component that prints "bar". */
    export class FooComponent {}

    ```

- Write the JsDoc block before the Decorator.

    ```ts
    /** Component that prints "bar". */
    @Component({
      selector: 'foo',
      template: 'bar',
    })
    export class FooComponent {}
    ```


# Language rules

These language rules are guidelines we _should_ follow, but there may exist
valid reasons or circumstances to ignore these conventions. The full
implications of ignoring these conventions must be understood and carefully
weighed before choosing a different course.

These rules have been written with typescript in mind, but should apply to other
languages as well.

## Brackets

K&R C (egyptian) style brackets should be used. If you encounter code that does
not follow this convention, please change it to match the chosen convention.

Valid reasons or circumstances can exist where using K&R reduces readability. In
these cases, other styles of bracket placement may be tolerated.

<!-- PAGE BREAK -->

## Visibility

Restricting visibility of properties, methods, and entire types helps with
keeping code decoupled.

- Denote symbol visibility as much as possible
- **Exception**: do not include the `public` modifier on the constructor of a
    class.
- Typescript are public by default. Please include the `public` modifier however
    for readability and preventing misconceptions.

```ts
// Bad
class Foo {
  bar = "default";
}

// Bad
class Foo {
  public constructor() {}
}

// Good
class Foo {
  public bar = "default";

  constructor() {
    // ...
  }
}

```

<!-- PAGE BREAK -->

## Constructors

Constructor calls must use parentheses, even when no arguments are passed.

```ts
// Bad
const x = new Foo;

// Good
const x = new Foo();
```

It is unnecessary to provide an empty constructor or one that simply delegates
into its parent class because ES2015 provides a default class constructor if one
is not specified. However constructors with parameter properties, modifiers or
parameter decorators should not be omitted even if the body of the constructor
is empty.

```ts
// Bad
class UnnecessaryConstructor {
  constructor() {}
}

// Bad
class UnnecessaryConstructorOverride extends Base {
    constructor(value: number) {
      super(value);
    }
}

// Good
class DefaultConstructor {
}

// Good
class ParameterProperties {
  constructor(private myService) {}
}

// Good
class ParameterDecorators {
  constructor(@SideEffectDecorator myService) {}
}

// Good
class NoInstantiation {
  private constructor() {}
}
```

<!-- PAGE BREAK -->

## Class Members

### No `#private` fields

Do not use private fields (also known as private identifiers)

```ts
// Bad
class Clazz {
  #ident = 1;
}
```

Instead, use visibility annotations

```ts
// Good
class Clazz {
  private ident = 1;
}
```

### Use `readonly`

Mark properties that are never reassigned outside of the constructor with the
readonly modifier (these need not be deeply immutable).

### Param properties

Rather than plumbing an obvious initializer through to a class member, use a
parameter property.

```ts
// Bad
class Foo {
  private readonly barService: BarService;

  constructor(barService: BarService) {
    this.barService = barService;
  }
}

// Good
class Foo {
  constructor(private readonly barService: BarService) {}
}
```

If the parameter property needs documentation, use an @param JSDoc tag.

Not all languages may implement this form of initializers, in which case the bad
example may be tolerated.

<!-- PAGE BREAK -->

### Field initializers

If a class member is not a parameter, initialize it where it's declared, which
sometimes lets you drop the constructor entirely.

```ts
// Bad
class Foo {
  private readonly userList: string[];
  constructor() {
    this.userList = [];
  }
}

// Good
class Foo {
  private readonly userList: string[] = [];
}
```

### Properties used outside of class lexical scope

Properties used from outside the lexical scope of their containing class, such
as an AngularJS controller's properties used from a template, must not use
`private` visibility, as they are used outside of the lexical scope of their
containing class.

Prefer `public` visibility for these properties, however `protected` visibility
can also be used as needed. For example, Angular and Polymer template properties
should use public, but AngularJS should use protected.

TypeScript code must not not use obj['foo'] to bypass the visibility of a
property.

Why?

When a property is `private`, you are declaring to both automated systems and
humans that the property accesses are scoped to the methods of the declaring
class, and they will rely on that. For example, a check for unused code will
flag a private property that appears to be unused, even if some other file
manages to bypass the visibility restriction.

Though it may appear that `obj['foo']` can bypass visibility in the TypeScript
compiler, this pattern can be broken by rearranging the build rules, and also
violates optimization compatibility.

<!-- PAGE BREAK -->

### Getters and Setters (Accessors)

Getters and setters for class members may be used. The getter method must be a
[pure function](https://en.wikipedia.org/wiki/Pure_function) (i.e., result is
consistent and has no side effects). They are also useful as a means of
restricting the visibility of internal or verbose implementation details (shown
below).

```ts
class Foo {
  constructor(private readonly someService: SomeService) {}

  public get someMember(): string {
    return this.someService.someVariable;
  }

  public set someMember(newValue: string) {
    this.someService.someVariable = newValue;
  }
}
```

If an accessor is used to hide a class property, the hidden property may be
prefixed or suffixed with any whole word, like `internal` or `wrapped`. When
using these private properties, access the value through the accessor whenever
possible. At least one accessor for a property must be non-trivial: do not
define "pass-through" accessors only for the purpose of hiding a property.
Instead, make the property public (or consider making it `readonly` rather than
just defining a getter with no setter).

```ts
// Bad
class Bar {
  private barInternal = '';
  // Neither of these accessors have logic, so just make bar public.
  public get bar() {
    return this.barInternal;
  }

  public set bar(value: string) {
    this.barInternal = value;
  }
}

// Good
class Foo {
  private wrappedBar = '';
  public get bar() {
    return this.wrappedBar || 'bar';
  }

  public set bar(wrapped: string) {
    this.wrappedBar = wrapped.trim();
  }
}
```

<!-- PAGE BREAK -->

## Primitive Types & Wrapper Classes

TypeScript code must not instantiate the wrapper classes for the primitive types
`String`, `Boolean`, and `Number`. Wrapper classes have surprising behaviour,
such as `new Boolean(false)` evaluating to `true`.

```ts
// Bad
const s = new String('hello');
const b = new Boolean(false);
const n = new Number(5);

// Good
const s = 'hello';
const b = false;
const n = 5;
```

## Array constructor

TypeScript code must not use the `Array()` constructor, with or without new. It
has confusing and contradictory usage.

```ts
// Bad
const a = new Array(2); // [undefined, undefined]
const b = new Array(2, 3); // [2, 3];
```

Instead, always use bracket notation to initialize arrays, or `from` to
initialize an Array with a certain size

```ts
const a = [2];
const b = [2, 3];

// Equivalent to Array(2):
const c = [];
c.length = 2;

// [0, 0, 0, 0, 0]
Array.from<number>({length: 5}).fill(0);
```

<!-- PAGE BREAK -->

## Type coercion

TypeScript code may use the `String()` and `Boolean()` (note: no `new`!)
functions, string template literals, or `!!` to coerce types.

```ts
const bool = Boolean(false);
const str = String(aNumber);
const bool2 = !!str;
const str2 = `result: ${bool2}`;
```

Using string concatenation to cast to string is discouraged, as we check that
operands to the plus operator are of matching types.

Code must use `Number()` to parse numeric values, and must check its return for
`NaN` values explicitly, unless failing to parse is impossible from context.

> Note: `Number('')`, `Number(' ')`, and `Number('\t')` would return `0` instead
> of `NaN`. `Number('Infinity')` and `Number('-Infinity')` would return
> `Infinity` and `-Infinity` respectively. These cases may require special
> handling.

```ts
const aNumber = Number('123');
if (isNaN(aNumber)) throw new Error(...);  // Handle NaN if the string might not
                                           // contain a number
assertFinite(aNumber, ...);                // Optional: if NaN cannot happen
                                           // because it was validated before.
```

Code must not use unary plus (`+`) to coerce strings to numbers. Parsing numbers
can fail, has surprising corner cases, and can be a code smell (parsing at the
wrong layer). A unary plus is too easy to miss in code reviews given this.

```ts
// Bad
const x = +y
```

Code must also not use `parseInt` or `parseFloat` to parse numbers, except for
non-base-10 strings (see below). Both of those functions ignore trailing
characters in the string, which can shadow error conditions (e.g. parsing `12
dwarves` as `12`).

```ts
// Bad
const n = parseInt(someString, 10);  // Error prone,
const f = parseFloat(someString);    // regardless of passing a radix.
```

Code that must parse using a radix _must_ check that its input is a number
before calling into `parseInt`;

```ts
if (!/^[a-fA-F0-9]+$/.test(someString)) throw new Error(...);
// Needed to parse hexadecimal.
// tslint:disable-next-line:ban
const n = parseInt(someString, 16);  // Only allowed for radix != 10
```

Use `Number()` followed by `Math.floor` or `Math.trunc` (where available) to
parse integer numbers:

```ts
let f = Number(someString);
if (isNaN(f)) handleError();
f = Math.floor(f);
```

<!-- PAGE BREAK -->

Do not use explicit boolean coercions in conditional clauses that have implicit
boolean coercion. Those are the conditions in an `if`, `for` and `while`
statements.

```ts
// Bad
const foo: MyInterface|null = ...;
if (!!foo) {...}
while (!!foo) {...}

// Good
const foo: MyInterface|null = ...;
if (foo) {...}
while (foo) {...}
```

Code may use explicit comparisons:

```ts
// Explicitly comparing > 0 is OK:
if (arr.length > 0) {...}
// so is relying on boolean coercion:
if (arr.length) {...}
```

## Variables

Always use `const` or `let` to declare variables. Use `const` by default, unless
a variable needs to be reassigned. Never use `var`.

```ts
const foo = otherValue;  // Use if "foo" never changes.
let bar = someValue;     // Use if "bar" is ever assigned into later on.
```

`const` and `let` are block scoped, like variables in most other languages.
`var` in JavaScript is function scoped, which can cause difficult to understand
bugs. Don't use it.

```ts
// Bad
var foo = someValue;     // Don't use - var scoping is complex and causes bugs.
```

Variables must not be used before their declaration.

## Exceptions

Always use `new Error()` when instantiating exceptions, instead of just calling
`Error()`. Both forms create a new `Error` instance, but using `new` is more
consistent with how other objects are instantiated.

```ts
// Bad
throw Error('Foo is not a valid bar.');

// Good
throw new Error('Foo is not a valid bar.');
```

<!-- PAGE BREAK -->

## Iterating objects

Iterating objects with `for (... in ...)` is error prone. It will include
enumerable properties from the prototype chain.

Do not use unfiltered `for (... in ...)` statements:

```ts
// Bad
for (const x in someObj) {
  // x could come from some parent prototype!
}
```

Either filter values explicitly with an `if` statement, or use
`for (... of Object.keys(...))`.

```ts
// Good
for (const x in someObj) {
  if (!someObj.hasOwnProperty(x)) continue;
  // now x was definitely defined on someObj
}
for (const x of Object.keys(someObj)) { // note: for _of_!
  // now x was definitely defined on someObj
}
for (const [key, value] of Object.entries(someObj)) { // note: for _of_!
  // now key was definitely defined on someObj
}
```

<!-- PAGE BREAK -->

## Iterating containers

Do not use `for (... in ...)` to iterate over arrays. It will counterintuitively
give the array's indices (as strings!), not values:

```ts
// Bad
for (const x in someArray) {
  // x is the index!
}
```

Use `for (... of someArr)` or vanilla `for` loops with indices to iterate over
arrays.

```ts
// Good
for (const x of someArr) {
  // x is a value of someArr.
}

for (let i = 0; i < someArr.length; i++) {
  // Explicitly count if the index is needed, otherwise use the for/of form.
  const x = someArr[i];
  // ...
}
for (const [i, x] of someArr.entries()) {
  // Alternative version of the above.
}
```

Do not use `Array.prototype.forEach`, `Set.prototype.forEach`, and
`Map.prototype.forEach`. They make code harder to debug and defeat some useful
compiler checks (e.g. reachability).

```ts
// Bad
someArr.forEach((item, index) => {
  someFn(item, index);
});
```

Why?

Consider this code:

```ts
let x: string|null = 'abc';
myArray.forEach(() => { x.charAt(0); });
```

You can recognize that this code is fine: `x` isn't null and it doesn't change
before it is accessed. But the compiler cannot know that this `.forEach()` call
doesn't hang on to the closure that was passed in and call it at some later
point, maybe after `x` was set to null, so it flags this code as an error. The
equivalent for-of loop is fine.

<!-- PAGE BREAK -->

## Using the spread operator

Using the spread operator `[...foo]`; `{...bar}` is a convenient shorthand for
copying arrays and objects. When using the spread operator on objects, later
values replace earlier values at the same key.

```ts
const foo = {
  num: 1,
};

const foo2 = {
  ...foo,
  num: 5,
};

const foo3 = {
  num: 5,
  ...foo,
}

foo2.num === 5;
foo3.num === 1;
```

When using the spread operator, the value being spread must match what is being
created. That is, when creating an object, only objects may be used with the
spread operator; when creating an array, only spread iterables. Primitives,
including `null` and `undefined`, may never be spread.

```ts
// Bad

const foo = {num: 7};
const bar = {num: 5, ...(shouldUseFoo && foo)}; // might be undefined

// Creates {0: 'a', 1: 'b', 2: 'c'} but has no length
const fooStrings = ['a', 'b', 'c'];
const ids = {...fooStrings};
```

```ts
// Good
const foo = shouldUseFoo ? {num: 7} : {};
const bar = {num: 5, ...foo};
const fooStrings = ['a', 'b', 'c'];
const ids = [...fooStrings, 'd', 'e'];
```

<!-- PAGE BREAK -->

## Control flow statements & blocks

Control flow statements spanning multiple lines always use blocks for the
containing code.

```ts
// Good

for (let i = 0; i < x; i++) {
  doSomethingWith(i);
  andSomeMore();
}
if (x) {
  doSomethingWithALongMethodName(x);
}

// Bad

if (x)
  x.doFoo();
for (let i = 0; i < x; i++)
  doSomethingWithALongMethodName(i);
```

The exception is that `if` statements fitting on one line _may_ elide the block.

```ts
if (x) x.doFoo();
```

## Switch statements

All `switch` statements must contain a `default` statement group, even if it
contains no code.

```ts
switch (x) {
  case Y:
    doSomethingElse();
    break;
  default:
    // nothing to do.
}
```

Non-empty statement groups (`case` ...) may not fall through. Use multiple
switch statements in series instead.

```ts
// Bad

switch (x) {
  case X:
    doSomething();
    // fall through - not allowed!
  case Y:
    // ...
}
```

Empty statement groups are allowed to fall through

```ts
// Good

switch (x) {
  case X:
  case Y:
    doSomething();
    break;
  default: // nothing to do.
}
```

<!-- PAGE BREAK -->

## Equality checks

Always use triple equals (`===`) and not equals (`!==`). The double equality
operators cause error prone type coercions that are hard to understand and
slower to implement for JavaScript Virtual Machines. See also the JavaScript
equality table.

When performing equality checks, place the readonly part of the check on the
left-hand side (yoda statements) whenever possible. Should an assignment (`=`)
be placed by accident instead of an equality check, this will throw an error by
the compiler or parser.

When multiple equality checks are being performed within a single `if`, `while`
or `for` statement, always include parentheses to clarify the order of checking,
even if they serve no functional purpose.

```ts
// Bad

if (foo == 'bar' || baz != bam) {
  // Hard to understand behaviour due to type coercion.
}

// Good
if (('bar' === foo) || (baz !== bam)) {
  // All good here.
}
```

**Exception**: Comparisons to the literal `null` value may use the `==` and `!=`
operators to cover both `null` and `undefined` values.

```ts
if (null == foo) {
  // Will trigger when foo is null or undefined.
}
```

<!-- PAGE BREAK -->

## Function declarations

Use `function foo() { ... }` to declare named functions, including functions in
nested scopes, e.g. within another function.

Use function declarations instead of assigning a function expression into a
local variable (`const x = function() {...};`). TypeScript already disallows
rebinding functions, so preventing overwriting a function declaration by using
`const` is unnecessary.

**Exception**: Use arrow functions assigned to variables instead of function declarations if the function accesses the outer scope's this.

```ts
function foo() { ... }

// Given the above declaration, this won't compile:
foo = () => 3;  // ERROR: Invalid left-hand side of assignment expression.

// So declarations like this are unnecessary.
const foo = function() { ... }
```

Note the difference between function declarations (`function foo() {}`)
discussed here, and function expressions (`doSomethingWith(function() {});`)
discussed below.

Top level arrow functions _may_ be used to explicitly declare that a function
implements an interface.

```ts
interface SearchFunction {
  (source: string, subString: string): boolean;
}

const fooSearch: SearchFunction = (source, subString) => { ... };
```

## Function expressions

### Use arrow functions in expressions

Always use arrow functions instead of pre-ES6 function expressions defined with
the `function` keyword.

```ts
// Bad
bar(function() { ... })

// Good
bar(() => { this.doSomething(); })
```

Function expressions (defined with the `function` keyword) may only be used if
code has to dynamically rebind the `this` pointer, but code _should not_ rebind
the `this` pointer in general. Code in regular functions (as opposed to arrow
functions and methods) should not access `this`.

<!-- PAGE BREAK -->

### Expression bodies vs block bodies

Use arrow functions with expressions or blocks as their body as appropriate.

```ts
// Top level functions use function declarations.
function someFunction() {
  // Block arrow function bodies, i.e. bodies with => { }, are fine:
  const receipts = books.map((b: Book) => {
    const receipt = payMoney(b.price);
    recordTransaction(receipt);
    return receipt;
  });

  // Expression bodies are fine, too, if the return value is used:
  const longThings = myValues.filter(v => v.length > 1000).map(v => String(v));

  function payMoney(amount: number) {
    // function declarations are fine, but don't access `this` in them.
  }
}
```

Only use an expression body if the return value of the function is actually
used.

```ts
// BAD: use a block ({ ... }) if the return value of the function is not used.
myPromise.then(v => console.log(v));

// GOOD: return value is unused, use a block body.
myPromise.then(v => {
  console.log(v);
});

// GOOD: code may use blocks for readability.
const transformed = [1, 2, 3].map(v => {
  const intermediate = someComplicatedExpr(v);
  const more = acrossManyLines(intermediate);
  return worthWrapping(more);
});
```

### Rebinding `this`

Function expressions must not use `this` unless they specifically exist to
rebind the `this` pointer. Rebinding `this` can in most cases be avoided by
using arrow functions or explicit parameters.

```ts
function clickHandler() {
  // Bad: what's `this` in this context?
  this.textContent = 'Hello';
}
// Bad: the `this` pointer reference is implicitly set to document.body.
document.body.onclick = clickHandler;

// Good: explicitly reference the object from an arrow function.
document.body.onclick = () => { document.body.textContent = 'hello'; };
// Alternatively: take an explicit parameter
const setTextFn = (e: HTMLElement) => { e.textContent = 'hello'; };
document.body.onclick = setTextFn.bind(null, document.body);
```

<!-- PAGE BREAK -->

### Arrow functions as properties

Classes usually _should_ not contain properties initialized to arrow functions.
Arrow function properties require the calling function to understand that the
callee's `this` is already bound, which increases confusion about what `this`
is, and call sites and references using such handlers look broken (i.e. require
non-local knowledge to determine that they are correct). Code should always use
arrow functions to call instance methods
(`const handler = (x) => { this.listener(x); };`), and _should not_ obtain or
pass references to instance methods
(`const handler = this.listener; handler(x);`).

```ts
// Bad

class DelayHandler {
  constructor() {
    // Problem: `this` is not preserved in the callback. `this` in the callback
    // will not be an instance of DelayHandler.
    setTimeout(this.patienceTracker, 5000);
  }
  private patienceTracker() {
    this.waitedPatiently = true;
  }
}

// Arrow functions usually should not be properties.
class DelayHandler {
  constructor() {
    // Bad: this code looks like it forgot to bind `this`.
    setTimeout(this.patienceTracker, 5000);
  }
  private patienceTracker = () => {
    this.waitedPatiently = true;
  }
}

// Good

// Explicitly manage `this` at call time.
class DelayHandler {
  constructor() {
    // Use anonymous functions if possible.
    setTimeout(() => {
      this.patienceTracker();
    }, 5000);
  }
  private patienceTracker() {
    this.waitedPatiently = true;
  }
}
```

<!-- PAGE BREAK -->

### Event handlers

Event handlers may use arrow functions when there is no need to uninstall the
handler (for example, if the event is emitted by the class itself). If the
handler must be uninstalled, arrow function properties are the right approach,
because they automatically capture `this` and provide a stable reference to
uninstall.

```ts
// Good

// Event handlers may be anonymous functions or arrow function properties.
class Component {
  onAttached() {
    // The event is emitted by this class, no need to uninstall.
    this.addEventListener('click', () => {
      this.listener();
    });
    // this.listener is a stable reference, we can uninstall it later.
    window.addEventListener('onbeforeunload', this.listener);
  }
  onDetached() {
    // The event is emitted by window. If we don't uninstall, this.listener will
    // keep a reference to `this` because it's bound, causing a memory leak.
    window.removeEventListener('onbeforeunload', this.listener);
  }
  // An arrow function stored in a property is bound to `this` automatically.
  private listener = () => {
    confirm('Do you want to exit the page?');
  }
}
```

Do not use `bind` in the expression that installs an event handler, because it
creates a temporary reference that can't be uninstalled.

```ts
// Bad

// Binding listeners creates a temporary reference that prevents uninstalling.
class Component {
  onAttached() {
    // This creates a temporary reference that we won't be able to uninstall
    window.addEventListener('onbeforeunload', this.listener.bind(this));
  }
  onDetached() {
    // This bind creates a different reference, so this line does nothing.
    window.removeEventListener('onbeforeunload', this.listener.bind(this));
  }
  private listener() {
    confirm('Do you want to exit the page?');
  }
}
```

## Automatic semicolon insertion

Do not rely on Automatic Semicolon Insertion (ASI). Explicitly terminate all
statements using a semicolon. This prevents bugs due to incorrect semicolon
insertions and ensures compatibility with tools with limited ASI support (e.g.
clang-format).

<!-- PAGE BREAK -->

## @ts-ignore

Do not use `@ts-ignore`. It superficially seems to be an easy way to "fix" a
compiler error, but in practice, a specific compiler error is often caused by a
larger problem that can be fixed more directly.

For example, if you are using `@ts-ignore` to suppress a type error, then it's
hard to predict what types the surrounding code will end up seeing.

## Type and non-nullability assertions

Type assertions (`x as SomeType`) and non-nullability assertions (`y!`) are
unsafe. Both only silence the TypeScript compiler, but do not insert any runtime
checks to match these assertions, so they can cause your program to crash at
runtime.

Because of this, you _should not_ use type and non-nullability assertions
without an obvious or explicit reason for doing so.

Instead of the following:

```ts
// Bad
(x as Foo).foo();

y!.bar();
```

When you want to assert a type or non-nullability the best answer is to
explicitly write a runtime check that performs that check.

```ts
// assuming Foo is a class.
if (x instanceof Foo) {
  x.foo();
}

if (y) {
  y.bar();
}
```

Sometimes due to some local property of your code you can be sure that the
assertion form is safe. In those situations, you should add clarification to
explain why you are ok with the unsafe behavior:

```ts
// x is a Foo, because ...
(x as Foo).foo();

// y cannot be null, because ...
y!.bar();
```

If the reasoning behind a type or non-nullability assertion is obvious, the
comments may not be necessary. For example, generated proto code is always
nullable, but perhaps it is well-known in the context of the code that certain
fields are always provided by the backend. Use your judgement.

<!-- PAGE BREAK -->

## Type assertion syntax

Type assertions must use the `as` syntax (as opposed to the angle brackets
syntax). This enforces parentheses around the assertion when accessing a
member.

```ts
// Bad
const x = (<Foo>z).length;
const y = <Foo>z.length;

// Good
const x = (z as Foo).length;
```

## Type Assertions and Object Literals

Use type annotations (`: Foo`) instead of type assertions (`as Foo`) to specify
the type of an object literal. This allows detecting refactoring bugs when the
fields of an interface change over time.

```ts
// Bad

interface Foo {
  bar: number;
  baz?: string;  // was "bam", but later renamed to "baz".
}

const foo = {
  bar: 123,
  bam: 'abc',  // no error!
} as Foo;

function func() {
  return {
    bar: 123,
    bam: 'abc',  // no error!
  } as Foo;
}

// Good

interface Foo {
  bar: number;
  baz?: string;
}

const foo: Foo = {
  bar: 123,
  bam: 'abc',  // complains about "bam" not being defined on Foo.
};

function func(): Foo {
  return {
    bar: 123,
    bam: 'abc',   // complains about "bam" not being defined on Foo.
  };
}
```

<!-- PAGE BREAK -->

## Member property declarations

Interface and class declarations must use the `;` character to separate
individual member declarations:

```ts
interface Foo {
  memberA: string;
  memberB: number;
}
```

Interfaces specifically must not use the `,` character to separate fields, for
symmetry with class declarations:

```ts
// Bad
interface Foo {
  memberA: string,
  memberB: number,
}
```

Inline object type declarations must use the comma as a separator:

```
type SomeTypeAlias = {
  memberA: string,
  memberB: number,
};

let someProperty: {memberC: string, memberD: number};
```

## Optimization compatibility for property access

Code must not mix quoted property access with dotted property access:

```ts
// Bad: code must use either non-quoted or quoted access for any property
// consistently across the entire application:
console.log(x['someField']);
console.log(x.someField);
```

Code must not rely on disabling renaming, but must rather declare all properties
that are external to the application to prevent renaming:

Prefer for code to account for a possible property-renaming optimization, and
declare all properties that are external to the application to prevent
renaming:

```ts
// Good: declaring an interface
declare interface ServerInfoJson {
  appVersion: string;
  user: UserJson;
}
const data = JSON.parse(serverResponse) as ServerInfoJson;
console.log(data.appVersion); // Type safe & renaming safe!
```

<!-- PAGE BREAK -->

## Optimization compatibility for module object imports

When importing a module object, directly access properties on the module object
rather than passing it around. This ensures that modules can be analyzed and
optimized. Treating module imports as namespaces is fine.

```ts
// Bad
import * as utils from 'utils';
class A {
  readonly utils = utils;
}

// Good
import {method1, method2} from 'utils';
class A {
  readonly utils = {method1, method2};
}
```

## Exception

This optimization compatibility rule applies to all web apps. It does not apply
to code that only runs server side (e.g. in NodeJS for a test runner). It is
still strongly encouraged to always declare all types and avoid mixing quoted
and unquoted property access, for code hygiene.

## Enums

Always use `enum` and not `const enum`. TypeScript enums already cannot be
mutated; `const enum` is a separate language feature related to optimization
that makes the enum invisible to JavaScript users of the module.

## Debugger statements

Debugger statements must not be included in production code.

## Decorators

Decorators are syntax with an `@` prefix, like `@MyDecorator`.

New decorators _should not_ be defined by the project itself, but rather by
frameworks and/or libraries used by the project. Try to avoid defining new
decorators.

Why?

Defining our own decorators removes the ability to use non-typed typescript
compilers like esbuild in projects. Libraries and frameworks usually include
already-compiled versions, allowing compilers like esbuild to function properly
without handling typing themselves.

```ts
/** JSDoc comments go before decorators */
@Component({...})  // Note: no empty line after the decorator.
class MyComp {
  @Input() myField: string;  // Decorators on fields may be on the same line...

  @Input()
  myOtherField: string;  // ... or wrap.
}
```

# Source Organization

This section defines conventions for organizing the source, it's layout naming

We _should_ use this same structure for all projects, regardless of whether the
project is barebones or based on top of a framework.

## Filesystem

Folders and files _should_ use kebab-case in all cases. In backend structures,
the type of the class contained in a file _should_ be denoted by using
dot-separation at the end of the filename, with the exception of model classes
(e.g. `.../model/user.ts` instead of `.../model/user.model.ts`).

Further explanation is given by example. If any part of this section is not
clear, feel free to contact the author(s) of this document and request more
clearly defined rules of more examples.

The reason for choosing kebab-case, is because we should assume any part of a
project may become it's own project one day. In npm, for example, packages are
not allowed to contain uppercase letters, which leaves kebab-case and
snake_case. Most packages on registered on npm today use kebab-case. The only
use for underscores is for internal node packages, and this is simple a
convention from the early days of node.js.

```
packages/
  module-name-a/
  module-name-b/
src/
  iam/
    iam.module.ts
    model/
      account.ts
      user.ts
      account-relation.ts
    resolver/
      account.resolver.ts
      user.resolver.ts
      account-relation.resolver.ts
    service/
      account.service.ts
      user.service.ts
      account-relation.service.ts
```

<!-- PAGE BREAK -->

## Project root

For most applications, we should set up a monorepo for the web side of the
project. Whether the mobile application resides in the same repository as the
web parts will be decided on a per-project basis.

```
api/                -->  backend code root
web/                -->  html-/web-based frontend root
lib/                -->  code common to both the backend and the web frontend
app/                -->  application frontend
  |-- common/       -->  cross-platform components
  |-- android/      -->  android root
  \-- ios/          -->  ios root
data/               -->  storage location for development (mysql, mongo, etc)
specifications/     -->  human-readable, project-specific, development rules
docker-compose.yml  -->  docker-compose file on how to run the development env
Makefile            -->  shortcuts and tools
README.md           -->  Quick overview/introduction of the project
```

While the lib folder is included in this overview, often the libraries or
packages built in it _should_ become their own package on npmjs.com (or wherever
the package manager expects them by default). Highly project-specific common
code _may_ be included in it though.

If the mobile application is purely cross-platform (using flutter, for example),
the platform layer of the app folder _may_ be skipped. Doing this, the app
folder becomes the project root for the mobile application.

Native applications, like for Windows or Mac OSX, are considered apps as well,
requiring them to be located at `/app/windows` and `/app/osx`.

Applications that are cross-platform but do not cover all supported platforms
_should_ be grouped by the type of platform they run on. An application having a
different codebase for desktop and mobile would have `/app/desktop` and
`/app/mobile` respectively.

Whenever possible, a `Dockerfile` _should_ be created in each codebase with
instructions on how to run that specific component in it's own docker container
in order to allow the whole project to be started from a `docker-compose.yml`
file in the project root. If the codebase is not compatible with docker (a
mobile application for example), instructions on how to run it must be present
in the `README.md` file in the root directory of the codebase.

<!-- PAGE BREAK -->

## Backend root

Most backend roots will look alike, no matter if they're nestjs-based or
barebones.

```
dist/               -->  compiled version of the source code
test/               -->  unit-/feature tests
src/                -->  sources (to be compiled)
  |-- config/       -->  semi-static configuration files
  |-- common/       -->  files shared between multiple components
  \-- module-name/  -->  single-responsibility module
Dockerfile          -->  instructions on how to dockerize the backend
tsconfig.json       -->  instructions on how to compile the backend
```

Backend frameworks like nest.js or sails.js have their own instructions on how
to structure the src directory. These instructions should be followed.

## Frontend root

Frontends, whether that be web or mobile, have greatly varying structures. Each
platform or framework has it's own requirements and preferences. As a rule of
thumb, the structure directed by the platform and/or framework used _should_ be
followed instead of trying to apply our own structure to it.

<!-- PAGE BREAK -->

## Library root

Library roots can exist as either a stand-alone repository or as a folder within
the `lib/` of an application, depending on whether they are general-purpose or
project-specific. In this section you'll find layouts for common library
structures.

### Cross-platform libraries

**TL;DR;** All bets are off..

Structure for cross-platform libraries may differ greatly, depending on their
use-case. If the library is a cross-platform library, the root should be split
into common and platform-specific code in whatever way makes sense for that
specific library and must be documented in the library's specifications folder.

### Multi-library repository

**TL;DR;** root/src = common, root/packages/&lt;name&gt; = specific

Some closely-tied libraries may exist within a single repository. These
repositories must have a destinction between common and
library-/package-specific components though.

This structure hasn't occurred very often, so this overview is still a
work-in-progress.

```
src/             -->  code that's shared between multiple libraries
test/            -->  tests common between multiple libraries (for symlinking)
packages/        -->  container folder for individual libraries/packages
  |-- prefix-a/  -->  root folder for package called "prefix-a"
  \-- prefix-b/  -->  root folder for package called "prefix-b"
README.md        -->  Quick overview/introduction of the project
```

### Single-purpose library

This section also covers the package-specific part of multi-library
repositories.

Extra requirements will be defined on a per-project basis, as the structure will
differ too greatly to define here.

```
src/       -->  source code
dist/      -->  compiled version of the source code, if compilation is used
README.md  -->  Quick overview/introduction of the project
```

# Informative References <a id="reference"></a>

{{>ref-asd0000.md}}

- **TSGUIDE**: "Google Typescript Style Guide"
  [&lt;https://google.github.io/styleguide/tsguide.html&gt;](https://google.github.io/styleguide/tsguide.html)<br/><br/>

# Acknowledgements

This document is an adaptation of Google's TypeScript Style Guid [TSGUIDE],
using it for the purpose of bootstrapping our own style guide.

Not all possible edge-cases have been described in this document. When a case
arises that has not been covered by this document, carefully weigh different
options to determine what makes sense in the context of the decision to be made.

Should more cases be described in this document, feel free to contact the
author(s) of this document.

# Author Information

{{>author-rbron-appvise.md}}
