# {{title}}

## Abstract

This document represents a Style Guide for repositories in the App-Vise
namespace and it's projects.

<!-- HALF PAGE -->

{{>license-cc-by-4.0.md}}
