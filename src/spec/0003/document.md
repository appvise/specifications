# Introduction

The AppVise default web-based stack includes google cloud with a couple of
optional components in use. In this internal guide, we'll go over common
configurations to set up the default stack and components used in it.

{{>keywords-asd-0000.md}}

# Getting started

As a common practice we set up 2 projects within google cloud to keep testing
and production environments separate. These could be merged to save a little on
costs, but the most savings would be on the database hosting.

We will not cover creating the Google Cloud projects themselves in this guide,
but we will explain differences between a production setup and a testing setup
whenever the need arises.

# Choosing components

For some parts of a project, multiple options are available as defaults, based
on what the requirements of the project are. Some/most of these choices are
described in this section.

## Database

We don't really care about which database we use internally, as long as mature
client libraries exist that can handle indexes and relations (whether through
joins or built on top).

For this reason, we come down to another 2 options:

- **Cloud SQL - MySQL**: Automatic storage scaling, backups, good support and
    not having to worry about the hardware makes this the default
    choice.<br/><br/>
- **Cloud SQL - PostGres**: If another component already requires postgres,
    which is common in open-source projects, this makes a fine second choice
    with pretty much the same advantages as MySQL on Cloud SQL

We have discussed options like FireStore, Couchbase and others internally, but a
preference for those has not been identified yet.

## Backend Runner

We have 3 runners which we support within AppVise:

- **Cloud Functions**: stand-alone handlers which can be used for small and/or
    standalone tasks. Examples include a configuration endpoint, lifecycle
    handlers for firebase's firestore, http-based cronjobs, etc.<br/><br/>
- **Cloud Run**: docker in the cloud, allows running pretty much anything with
    the advantage of auto-scaling when load gets high.<br/><br/>
- **Nomad on Compute**: Fallback when the above 2 runners aren't sufficient.

**Cloud Functions** shouldn't be used for large tasks, as these are designed to
be short-lived for things like lifecycle events or triggering larger tasks.
Requests are limited to 10 MB, anything over that is not supported. If a task
takes longer than a second on here, you should probably think about splitting it
up into smaller tasks or moving it elsewhere. This is the preferred platform for
triggering background tasks, small stand-alone components and firebase lifecycle
event handlers.

**Cloud Run** can be used for the api itself as well as for background tasks. If
no streaming/chunked transfers are implemented, requests are limited to 32 MB.
Other than this limit, pretty much anything that can be dockerized can be run
here, making it the preferred platform for background tasks and the core api of
an application.

**Nomad on Compute** is definitely the outlier here, requiring more work to set
up and maintain. Auto-scaling is not supported here by default, but any workload
you can run on a linux machine can be run using it. This is the fallback
platform for when something refuses to run on the above 2 platforms or is hard
to set up using them.

For documentation on how to set up nomad, please check out [ASD0001][ASD0001].

## Frontend web host

A web frontend could be hosted on multiple platforms. We usually strive to have
the frontend support static hosting (pre-rendered/-compiled).

- **Cloud Storage**: Buckets can be used for static hosting, allowing simple
    access to assets for frontend developers and designers as well. Care should
    be taken to not expose the write-interface of the storage though.<br/><br/>
- **Cloud Run**: A docker image should be created and server-side
    rendering is supported fully here.

Up until now, we've used Cloud Run for hosting the frontend webservers. In the
future, we _should_ use Cloud Storage instead for static hosting whenever
possible, as the costs are lower for the customer (not running a container 24/7)
and makes use of Google's large scale Content Delivery Network.

# Setting up components

This section will cover the individual components. It is up to you to keep track
of which combination you've chosen, as we will not cover every possibility in
this guide.

TODO: More explanation here?

<!-- PAGE BREAK -->

## Database

### Cloud SQL - MySQL

TODO: convert following into nice representation

- create instance
  - gcloud console &gt; menu &gt; sql
  - choose mysql (may need 'create new instance')
  - give fancy id/name
  - generate random root password
  - select version (probably 5.7 or 8.0)
  - select region (europe-west4 = netherlands)
  - single-zone (testing) or multi-zone (prod)
  - customize instance
  - shared core, 0.6/1.7G (testing) or higher to needs (prod)
  - storage
    - hdd (testing) or ssd (prod)
    - capacity 10G + automatic storage increases
  - enable private IP
    - default network (or custom, if you know what you're doing)
    - set up connection (follow steps in popup)
  - enable backups if not already done
  - select maintenance window (outside of expected busy period)
  - add any flags/labels you require on the machines
  - create instance
- configure database
  - add user for api to use (if applicable)
  - create database
- write down DB_URL=mysql://username:password@internal-ip/dbname

<!-- PAGE BREAK -->

### Cloud SQL - PostgreSQL

TODO: convert following into nice representation

- create instance
  - gcloud console &gt; menu &gt; sql
  - choose postgresql (may need 'create new instance')
  - give fancy id/name
  - generate random root password
  - select version (probably 13)
  - select region (europe-west4 = netherlands)
  - single-zone (testing) or multi-zone (prod)
  - customize instance
  - shared core, 0.6/1.7G (testing) or higher to needs (prod)
  - storage
    - hdd (testing) or ssd (prod)
    - capacity 10G + automatic storage increases
  - enable private IP
    - default network (or custom, if you know what you're doing)
    - set up connection (follow steps in popup)
  - enable backups if not already done
  - select maintenance window (outside of expected busy period)
  - add any flags/labels you require on the machines
  - create instance
- configure database
  - add user for api to use (if applicable)
  - create database
- write down DB_URL=pgql://username:password@internal-ip/dbname

<!-- PAGE BREAK -->

## Backend
### Cloud Functions

TODO

### Cloud Run

TODO: convert following into nice representation

Add dockerfile to backend codebase:

```
FROM node:lts-alpine

COPY . /app
WORKDIR /app
RUN npm install
RUN npm run build --if-present

CMD npm run start
```

- gcloud console &gt; menu &gt; cloud run
- create service
- "continuously deploy new revisions from a source repository"
- set up with cloud build
- authenticate with github or bitbucket (prefer using service account on both gcloud and repo host)
- select repository
- select branch (probably `^main$` or `^master$`)
- set dockerfile path (probably `/api/Dockerfile`)
- minimum instances (0 = test, 1+ = prod)
- allow unauthenticated (unless specific needs)
- set memory/cpu if more/less than 512mb or 1vCPU needed per container
- set any env vars required by application (like DB_URL to connect to database)

<!-- PAGE BREAK -->

### Nomad

#### Cluster

Setting up the cluster itself has already been covered in [ASD0001][ASD0001] and
will not be explained in detail in this guide. ASD 0001 does assume you already
have machines, and this guide will assume you already know how to create virtual
machines in a single region/zone.

**important**: using the nomad guide, remove the `tls` entry from the caddy
configuration is you want google to handle the certificates instead.

Auto-scaling the machines using a managed group is not covered in this guide
either, but keep in mind you need an odd number of orchestrators to make
recovery easier and at least a single worker.

#### Load balancer

TODO: convert following list into nice textual explanation

- gcloud console -&gt; menu -&gt; network services -&gt; load balancing
- create loadbalancer
- choose type
  - http(s): we'll let google handle the ssl certificates
    - internet facing
    - classic
    - ??
  - tcp: we'll handle the ssl certificates ourselves (e.g. caddy)
    - internet facing
    - choose same region as your cluster
    - target pool or target instance
    - backend
      - select existing instances (select dst port 80+443 here?)
      - create http healthcheck (match route in api?)
      - choose session affinity (if stateful)
    - frontend
      - name http
        - static ip
        - port 80 in
      - name https
        - same ip as http
        - port 443 in

<!-- PAGE BREAK -->

## Frontend
### Cloud Storage

TODO: convert following into nice representation

https://cloud.google.com/storage/docs/hosting-static-website#objectives

### Cloud Run

TODO: convert following into nice representation

Add dockerfile to frontend codebase:

```
FROM node:lts-alpine as builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM node:lts-alpine as server
WORKDIR /app
RUN npm install -g http-server
COPY --from=builder /app/dist /app/dist
CMD [ "http-server", "dist" ]
```

OR

```
FROM finwo/quark:latest as server
COPY [--from=builder] <compiled-files> /srv/www
```

- gcloud console &gt; menu &gt; cloud run
- create service
- "continuously deploy new revisions from a source repository"
- set up with cloud build
- authenticate with github or bitbucket (prefer using service account on both gcloud and repo host)
- select repository
- select branch (probably `^main$` or `^master$`)
- set dockerfile path (probably `/web/Dockerfile`)
- minimum instances (0 = test, 1+ = prod)
- allow unauthenticated (unless specific needs)
- set memory/cpu if more/less than 512mb (128 is plenty for quark) or 1vCPU needed
- set any env vars required by dockerfile if custom

# Informative References <a id="reference"></a>

{{>ref-asd0000.md}}
{{>ref-asd0001.md}}

# Author Information

{{>author-rbron-appvise.md}}
