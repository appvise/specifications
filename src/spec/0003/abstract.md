# {{title}}

## Abstract

This document describes configurations to make and steps to take to set up the
default stack in use by AppVise with google cloud and how to operate it.

<!-- HALF PAGE -->

{{>license-cc-by-4.0.md}}
