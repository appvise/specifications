# {{title}}

## Abstract

This document describes the steps used to set up a nomad server cluster for an
earlier project and should act as a guide for setting up the same type of
cluster for other projects or our own infrastructure.

<!-- HALF PAGE -->

{{>license-cc-by-4.0.md}}
