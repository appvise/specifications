# Introduction

Application clusters can be hard to set up properly and manage. Multiple
frameworks for management are available, but almost all are easy to
mis-configure.

This document describes the steps used to set up a nomad server cluster for an
earlier project and should act as a guide for setting up the same type of
cluster for other projects or our own infrastructure.

Following this document as a guide should result in a set of machines that is
able to run docker workloads and bare executables. Should one of the machines
stop working, a different machine of the set should pick up the workload.

{{>keywords-asd-0000.md}}

# Preparing your workspace

There are some minor requirements for your local environment, namely a couple of
tools which we'll require.

## ssh

Your machine will have to support ssh and have set up a identity key (usually
something like id_rsa[.pub]), as we'll be connecting to the machines in the
cluster over ssh.

Creating a new ssh key can be performed using the following command and
following the steps presented by it:

```
ssh-keygen -t ed25519 -C "KEY COMMENT"
```

## hashi-up

For a quick setup of services, we'll be using the [hashi-up][hashi-up] tool to
install services on the cluster machines.

```
curl -sLS https://get.hashi-up.dev | sh
sudo install hashi-up /usr/local/bin/

# Check it's properly installed
hashi-up version
```

## ClusterSSH

Because there will be some operations that require modifications on all machines
at the same time, I recommend using
[ClusterSSH](https://github.com/duncs/clusterssh) to perform the exact same
commands on all machines.

If the installation on your particular operating system is difficult, feel free
to update this section to include those steps.

```sh
# Ubuntu
sudo apt-get install clusterssh

# Gentoo
sudo emerge -av net-misc/clusterssh
```

<!-- PAGE BREAK -->

# Setting up hardware

To setup a cluster, you off-course need servers/machines to run it on. For the
sake of this tutorial, let's assume the following setup exists:

- Machines
  - 3x Ubuntu 20.04, short names node-{00..02}
- Network
  - The machines have static IP addresses within the local network
  - Each machine has a secondary hard-drive for shared storage
  - The machines can ping eachother on their local network
  - Hostnames should have a structure like
    `<project>-<cluster>-<category>-<number>`, (e.g. `mpl-dc1-node-00`)

This document will not specify how to set up a public IP address with a
load-balancer, as that is highly dependent on the location of the machines
(cloud, vps, dedicated rack, etc) and therefor outside of the scope of this
document.

Once you've installed the machines (whether that's on a cloud provider or
physically in a rack/cupboard), you'll need to copy your public ssh key to these
machines: `ssh-copy-id <USERNAME>@<MACHINE>`

## Aliases

To make our life easier later in this document, you should add the ip addresses
(whether that's the public or local address is up to you) for these machines in
either your hosts file or in your ssh config.

<!-- PAGE BREAK -->

# Gluster

This section will assume all 3 machines will have a secondary hard-drive
registered as `/dev/sdb`

## Format storage disk

We do not need any fancy configuration for the storage disk, we just need a
linux-compatible filesystem on it which gluster can use to handle
synchronization and redundancy.

For sake of simplicity, we will not be running a partition table on the drive,
but instead place the filsystem directly on the drive itself.

```sh
mkfs.ext4 /dev/sdb
mkdir -p /mnt/brick-0
echo "/dev/sdb  /mnt/brick-0  ext4  defaults,noatime  0 0" >> /etc/fstab
mount /mnt/brick-0
```

## Install

Using clusterssh to connect to all storage machines
(`cssh mpl-dc1-node-{00..02}`), run the following commands under root:

```sh
apt install glusterfs-{client,server}
systemctl enable glusterd
systemctl start glusterd
```

## Cluster

Now that all machines have gluster installed, perform the following commands on
a single storage machine:

```sh
gluster peer probe node-00
gluster peer probe node-01
gluster peer probe node-02
gluster peer status
```

Hostnames should be used instead of ip addresses, especially if your machines
use dynamic local ip addresses for whatever reason.

After running this command you should see an overview of 3 machines registered
in gluster, indicating all are alive. Should this not be the case, feel free to
contact this document's author(s) to gather information on how to troubleshoot
the issue.

<!-- PAGE BREAK -->
<!-- Section crossed page boundary -->

## Volume

On the same machine that you initialized the cluster on (not required, just
assuming you still have a shell open there), use the following commands to set
up a volume on the new gluster cluster:

```sh
gluster volume create <VOLUME_NAME> \
  replica 3 \
  stripe 1 \
  node-{00..02}:/mnt/brick-0/<VOLUME_NAME>
gluster volume start <VOLUME_NAME>
gluster volume bitrot <VOLUME_NAME> enable
gluster volume status <VOLUME_NAME>
```

The amount of bricks you use within the cluster must be a multiple of the stripe
and replica numbers. So if you want 2 replicas and 2 stripes, you'll need 4, 8,
12, etc servers. It's advisable to keep an odd number of replicas (3/5) to make
your life easier when a section of your cluster goes down.

## Mount

Even though the volume should now be up-and-running, we will still need to mount
it so we can actually access the data on the volume. The easiest way to go about
this would be to run the following commands on all storage nodes:

```sh
mkdir -p /mnt/<VOLUME_NAME>
echo "localhost:/<VOLUME_NAME>" \
  "/mnt/<VOLUME_NAME>" \
  "glusterfs" \
  "defaults,_netdev" \
  "0 0" >> /etc/fstab
mount /mnt/<VOLUME_NAME>
```

As a test, you can create a file on one machine
`touch /mnt/<VOLUME_NAME>/test-file` and you should be able to see that file on
the other machines faster than you can type `ls`.

<!-- PAGE BREAK -->
<!-- Section crossed page boundary -->

# Optional: Keepalived

The storage cluster we've just created is available on the IP addresses of the
individual machines, but not on a common IP which has failover for when a
machine crashes. For this we'll set up kepealived which will handle a single IP
which will always point to a storage node.

## Install

If installing on your particular operating system is harder than expected,
please feel free to share those instructions in this section of this document.

On all machines, run the following code:

```sh
# On ubuntu
apt install keepalived
```

## Configuration

We'll set up a virtual router within keepalived to create the failover-ip
192.168.0.254, which will point to one of the servers containing this keepalived
config.

Please keep in mind that not all hosting platforms (Google Cloud for example)
support keepalived properly.

```
# /etc/keepalived/keepalived.conf

vrrp_instance VI_1 {
    state BACKUP
    interface <LOCAL_PHY>
    virtual_router_id 51
    priority 100
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass <SUPER_SECRET>
    }
    virtual_ipaddress {
        192.168.0.254/24
    }
}
```

<!-- PAGE BREAK -->

# Docker

Most machines used for hosting have a small OS drive, which in our case may
become a problem if we don't configure docker correctly.

## Storage

By default, docker stores all it's data in `/var/lib/docker`, which normally
resides on the OS drive. We will be storing this folder on our storage drive as
well, to prevent running into issues quickly regarding a full OS drive.

Let's first create that storage folder before we install docker on the nodes:

```
mkdir -p /mnt/brick-0/docker
mkdir -p /var/lib/docker

echo "/mnt/brick-0/docker" \
  "/var/lib/docker" \
  "none" \
  "bind,_netdev" \
  "0 0" \
  >> /etc/fstab

mount /var/lib/docker
```

## Install

After setting up this larger storage for docker, we will install an up-to-date
version of docker on the machine. This document will assume you're using ubuntu,
but on your specific distribution this may differ.

This section is a summary of https://docs.docker.com/engine/install/ubuntu/

```
apt update

apt install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | \
   gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture)" \
  "signed-by=/usr/share/keyrings/docker-archive-keyring.gpg]" \
  "https://download.docker.com/linux/ubuntu" \
  "$(lsb_release -cs) stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update

apt install docker-ce docker-ce-cli containerd.io

service docker enable
service docker start
```

## Authentication

Because we will later make nomad use the authentication by docker, now is a good
time to authenticate your docker with the docker hub or your private repository.

If you're running on google cloud, the gcloud sdk should already be installed on
your machine, allowing you to run `gcloud auth configure-docker`. If you want to
authenticate with a different repository, please look up the documentation for
the repository you want to use on how to authenticate your docker daemon.

# Consul

In this section, you will need to have passwordless access with ssh to your
machines using ssh keys. This document will not cover how to set that up.

To start setting up the cluster for handling workloads and other services, we
will be using the `hashi-up` installed in the beginning of the document.

## Install

```sh
# CAUTION: this has a retry to catch network hiccups
# THIS WILL LOOP IF YOU HAVE OTHER ERRORS
for machine in $(echo "mpl-dc1-node-00 mpl-dc1-node-01 mpl-dc1-node-02"); do
  while true; do
    hashi-up consul install \
      --ssh-target-addr $machine \
      --ssh-target-user <SSH_USER> \
      --ssh-target-key <SSH_KEY> \
      --client-addr 0.0.0.0 \
      --server \
      --bootstrap-expect 3 \
      --retry-join node-00 \
      --retry-join node-01 \
      --retry-join node-02 \
      && break
  done
done
```

Running this will automagically setup consul on all machines and recover the
consul cluster upon restart of any machine. Assuming port 8500 is blocked in the
firewall of either the environment or on the machine itself, this will not open
up the cluster to any machine on the internet, only to local instances.

If your machine is directly accessible from the internet, please make sure you
set up a firewall on the machine which blocks access to all ports other than 22,
80 and 443.

<!-- PAGE BREAK -->

## Configuration

To limit somewhat who is allowed to connect to the consul api, including the
internal network, we need to modify the addresses allowed in
`/etc/consul.d/consul.hcl`.

To allow local connections (nomad & ssh tunnels) & to allow docker containers on
the host (any other management tools) to use consul's api, we'll need to change
`client_asddr` into `"127.0.0.1 {{GetInterfaceIP \"docker0\"}}"`.

```
# /etc/consul.d/consul.hcl
# generated with hashi-up

datacenter  = "mpl-dc1"
data_dir    = "/opt/consul"
bind_addr   = "{{GetInterfaceIP \"ens4\"}}"
client_addr = "127.0.0.1 {{GetInterfaceIP \"docker0\"}}"
retry_join  = ["node-00", "node-01", "node-02"]
ports {
}
addresses {
}
ui               = true
server           = true
bootstrap_expect = 3
```

After modifying it's configuration, you will need to restart the consul service:

```
service consul restart
```

# Nomad

Now that consul exists across the machines in the cluster, we can set up nomad,
which is a workload orchestrator which can handle both docker and raw
executables.

## Install

Because consul already exists on the machines, the initial setup doesn't require
many arguments

```sh
# CAUTION: this has a retry to catch network hiccups
# THIS WILL LOOP IF YOU HAVE OTHER ERRORS
for machine in $(echo "mpl-dc1-nomad-00 mpl-dc1-nomad-01 mpl-dc1-nomad-02"); do
  while true; do
    hashi-up nomad install \
      --ssh-target-addr $machine \
      --ssh-target-user <SSH_USER> \
      --ssh-target-key <SSH_KEY> \
      --server \
      --bootstrap-expect 3 \
      && break
  done
done
```

## Configuration

While this installation works, we will need some extra configuration to actually
run workloads on these machines as well (or create separate "worker" machines).

Below is an excerpt of how you could set it up, but specific values may differ
for your specific cluster. Most important are the `client.enabled` key and the
docker authentication being added, to allow access to private repositories.

If `/root/.docker/config.json` doesn't exist yet, just create it with an empty
object as contents. If you've set up authentication for docker like described
earlier in this document, it should already exist.

```
# /etc/nomad.d/nomad.hcl
# generated with hashi-up

datacenter = "mpl-dc1"
data_dir   = "/opt/nomad"

# This machine is an orchestrator
server {
  enabled = true
  bootstrap_expect = 1
}

# This machine is a worker
client {
  enabled = true
}

# Allow running processes on worker directly through nomad
plugin "raw_exec" {
  config {
    enabled = true
  }
}


# Allow running docker images through nomad
plugin "docker" {
  config {
    auth {
      config = "/root/.docker/config.json"
    }
    volumes {
      enabled = true
    }
  }
}
```

# Vault

Now that we have a workload orchestrator, we need a way of managing secrets
throughout workloads.

## Install

Because consul already exists on the machines, the initial setup doesn't require
many arguments.

```sh
# CAUTION: this has a retry to catch network hiccups
# THIS WILL LOOP IF YOU HAVE OTHER ERRORS
for machine in $(echo "mpl-dc1-nomad-00 mpl-dc1-nomad-01 mpl-dc1-nomad-02"); do
  while true; do
    hashi-up vault install \
      --ssh-target-addr $machine \
      --ssh-target-user <SSH_USER> \
      --ssh-target-key <SSH_KEY> \
      --storage consul \
      --api-addr http://127.0.0.1:8200 \
      && break
  done
done
```

## Unseal manually

The initial setup of a vault will require you to enter the web-ui of vault,
which you can do using the following steps:

- ssh mpl-dc1-nomad-00 -L 8200:127.0.0.1:8200
- Go to http://127.0.0.1:8200 using your browser
- Follow the initial setup steps
- Download the unsealing key you're presented with

Store this key in a secure location (like maybe a password manager), and make
sure you always have access to ssh + port mapping for if a server needs to be
restarted (or crashes).

## Unseal automatically

If you want the vault to be unsealed (still needs authentication), you can also
add the unseal action as a cronjob to each server. Though it may not be as
secure, it will prevent your cluster from grinding to a halt when reboots start
rolling through your cluster.

Using cssh to connect to all mpl-dc1-nomad-{00..02} again, store the following
in `/etc/cron.d/vault-unseal`:

```
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
VAULT_ADDR=http://127.0.0.1:8200

* * * * *  root  vault operator unseal <UNSEAL_KEY>
```

# Caddy

Your cluster should be up-and-running by now, but services hosted within are not
yet accessible from world outside of your cluster/consul. We will be using
`Caddy` and `consul-template` to set up an ssl-offloading reverse proxy, which
will handle going from port 80/443 into your services.

## Install

We'll give an example using the ubuntu method of installing caddy, but you're
probably best of researching
[caddyserver.com/docs/install](https://caddyserver.com/docs/install) if you want
to install on a different operating system.

```sh
sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | \
  sudo tee /etc/apt/trusted.gpg.d/caddy-stable.asc
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | \
  sudo tee /etc/apt/sources.list.d/caddy-stable.list
sudo apt update
sudo apt install caddy
```

Next up is installing consul-template, which we'll be using to convert our
consul services into a Caddyfile.

- Go to [https://releases.hashicorp.com/consul-template/][consul-template-releases]
- Choose the most recent version
- Download the archive for your system
- Extract the archive
- Move the `consul-template` executable to a location you can execute from
  (that'd be `/usr/local/bin` in most linux distros)

## Configuration

### Caddy.service

This section describes how to modify the caddy service to watch for changes
itself. This can be skipped if the optional part of the caddyfile.service
section is added.

Because we'll be regenerating `/etc/caddy/Caddyfile` while the service is
running, we'll need to modify how caddy is run by the service manager. For our
case with ubuntu/systemd, we'll be modifying `/lib/systemd/system/caddy.service`
to make caddy watch for changes.

At the end of the `ExecStart` line, add `--watch` to the command. This will make
caddy watch for changes to `/etc/caddy/Caddyfile` which will reload the
configuration when it has changed.

```
# /lib/systemd/system/caddy.service

[Service]
...
ExecStart=/usr/bin/caddy run --environ --config /etc/caddy/Caddyfile --watch
...
```

<!-- PAGE BREAK -->

### Caddyfile.service

The most important section of the virtual host handler is the config, which will
be (re)generated by this service we'll manually create.

Create the `/etc/systemd/system/caddyfile.service` file, which we'll fill with
the following content:

```
# /etc/systemd/system/caddyfile.service

[Unit]
Description="Caddyfile consul listener
StartLimitInterval=0

[Service]
Restart=always
RestartSec=5
Type=simple
User=root
ExecStart=/usr/local/bin/consul-template -template "/etc/caddy/Caddyfile.tmpl:/etc/caddy/Caddyfile"

[Install]
WantedBy=multi-user.target
```

_To make `consul-template` trigger the caddy config reload, instead of relying
on the watch mechanism inside caddy (.service file changes may be overwritten by
a package update), you can add [`:caddy reload -config /etc/caddy/Caddyfile`] at
the end of the template argument. This addition will make consul-template run
that command each time the target-file was updated, making caddy reload it's
configuration._

We've defined `/etc/caddy/Caddyfile.tmpl` in the service file to be merged with
data and printed into `/etc/caddy/Caddyfile`, but now we need to actually create
the template:

```
# /etc/caddy/Caddyfile.tmpl

{{ range $tag, $services := service "web" | byTag }}{{ if $tag | regexMatch "VHOST=([.a-z]+)" }}
{{ $tag | regexReplaceAll "VHOST=([.a-z]+)" "$1" }} {
  tls info@app-vise.nl
  reverse_proxy{{ range $services }} {{ .Address }}:{{ .Port }}{{end}}
}{{end}}{{end}}
```

This template will look through all services named "web" within consul, look for
a tag on the service like `VHOST=<DOMAIN>`, and then generate a section within
the new Caddyfile dedicated to that domain. If you want more than 1 domain
linking to a service, simply add more tags to that service.

If you have other services which aren't registered in consul as web+vhost, you
should be able to add them in the caddyfile template using the following
structure:

```
...

DOMAIN NAME {
  tls info@app-vise.nl
  reverse_proxy IP:PORT [IP:PORT ...]
}
```

<!-- PAGE BREAK -->

## Resilience

Even though 'restart=always' is present in the service file, systemd may be
flaky with restarting the service when it stops (which may happen due to a node
crashing). To work around this, simply add the following cronjob to your cron.d.

If you're running an init scheme like `OpenRC`, you may want to set up a
watchdog instead, or if you're running `runit` you don't need any watchdog as
it's built in already.

```
# /etc/cron.d/caddyfile-restart

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

* * * * *   root    systemctl status caddyfile || systemctl start caddyfile
```

## Finishing up

Now that both services have been modified/configured, we need to reload our
system service runner and enable the services:

```sh
systemctl daemon-reload
systemctl start caddy
systemctl start caddyfile
systemctl enable caddy
systemctl enable caddyfile
service cron reload
```

Don't forget to resolve any errors thrown during these last commands, they
should indicate what's wrong with your newly-created reverse proxy.

# Deploying a service

Like with most workload orchestrators, multiple methods of deploying an
application are available.

The first method would be to ssh into 1 of the nomad machines, create the
following configuration file at `/mnt/nomad/jobs/<SERVICE_NAME>.nomad` and run
`nomad run <SERVICE_NAME>.nomad` to deploy it.

This example configuration file shows an "under construction" page on the
"example.com" domain (which should be modified to your needs).

More example can also be found in
[github.com/angrycub/nomad-example_jobs](https://github.com/angrycub/nomad_example_jobs),
but keep in mind that you'll need to add the `VHOST=<DOMAIN>` tag to work with
our setup for virtual hosts and ssl termination.

<!-- PAGE BREAK -->

```
# mpl-dc1-nomad-00:/mnt/nomad/jobs/under-construction.nomad

# There can only be a single job definition per file.
# Create a job with ID and Name 'example'
job "under-construction" {
  # Run the job in the global region, which is the default.
  # region = "global"

  # Specify the datacenters within the region this job can run in.
  datacenters = ["mpl-dc1"]

  # Service type jobs optimize for long-lived services. This is
  # the default but we can change to batch for short-lived tasks.
  type = "service"
  # type = "system"

  # Priority controls our access to resources and scheduling priority.
  # This can be 1 to 100, inclusively, and defaults to 50.
  # priority = 50

  # Restrict our job to only linux. We can specify multiple
  # constraints as needed.
  constraint {
    attribute = "${attr.kernel.name}"
    value     = "linux"
  }

  # Configure the job to do rolling updates
  update {
    # Stagger updates every 10 seconds
    stagger = "10s"

    # Update a single task at a time
    max_parallel = 1
  }

  # Create a 'cache' group. Each task in the group will be
  # scheduled onto the same machine.
  group "grp" {
    # Control the number of instances of this group.
    # Defaults to 1
    count = 1

    network {
      port "http" {}
    }

    reschedule {
      delay          = "30s"
      delay_function = "constant"
      unlimited      = true
    }

    # Define a task to run
    task "under-construction" {
      # Use Docker to run the task.
      driver = "docker"

      # environment variables
      env {
        PORT = "${NOMAD_PORT_http}"
      }

      # Configure Docker driver with the image
      config {
        image = "finwo/under-construction"
        ports = ["http"]
      }

      service {
        name = "web"
        tags = ["global", "VHOST=example.com"]
        port = "http"
        check {
          name     = "alive"
          type     = "http"
          interval = "10s"
          timeout  = "3s"
          path     = "/healthcheck"
        }
      }

      # Specify configuration related to log rotation
      logs {
          max_files     = 10
          max_file_size = 15
      }

      # Controls the timeout between signalling a task it will be killed
      # and killing the task. If not set a default is used.
      kill_timeout = "10s"

      # Limits/reserves of the resources for this task
      resources {
        cpu    = 1800
        memory = 768
      }
    }
  }
}
```

<!-- PAGE BREAK -->

# Security Considerations

The security considerations within this document have not been properly recorded
**YET**, but security considerations do exist within this document.

# Informative References

{{>ref-asd0000.md}}

- **hashi-up**: Siebens, J., "Bootstrap HashiCorp Consul, Nomad or Vault over SSH &lt; 1 minute"<br/>
  [&lt;https://github.com/jsiebens/hashi-up&gt;][hashi-up]<br/><br/>

- **consul-template**: HashiCorp, "Template rendering, notifier, and supervisor for @hashicorp Consul and Vault data"<br/>
  [&lt;https://github.com/hashicorp/consul-template&gt;][consul-template]<br/><br/>

[hashi-up]: https://github.com/jsiebens/hashi-up
[consul-template]: https://github.com/hashicorp/consul-template
[consul-template-releases]: https://releases.hashicorp.com/consul-template/

# Author Information

{{>author-rbron-appvise.md}}
