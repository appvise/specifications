# {{title}}

## Abstract

This document describes the fundamental and unique style conventions and
editorial policies currently in use for the Specification Series. It offers
guidance regarding the style and structure of a Specification.

<!-- HALF PAGE -->

{{>license-cc-by-4.0.md}}
